'use strict'

const express = require('express');
const app = express();
const axios = require('axios');
const appUrl = 'http://localhost:8081';
const apiUrl = '/url';

const imageDataRepo = [
  {
    type: 1,
    apiKey: 'CtCQ43XDe3X2Jpg8t266Fo7L5uWh6yIw',
    url: 'http://api.giphy.com/v1/gifs/search',
    getUrl: function (searchValue) {
      return `${this.url}?api_key=${this.apiKey}&q=${searchValue}`
    },
    fetchResults: function (results) {
      let resultsArr = [];
      results.data.forEach(function (result, index) {
        resultsArr.push(result.images['fixed_height_still'].url);
      })
      return resultsArr;
    }
  },
  {
    type: 2,
    apiKey: '13096761-6610712cc822775498378aec5',
    url: 'https://pixabay.com/api/',
    getUrl: function (searchValue) {
      return `${this.url}?key=${this.apiKey}&q=${searchValue}`
    },
    fetchResults: function (results) {
      let resultsArr = [];
      results['hits'].forEach(function (result, index) {
        resultsArr.push(result['webformatURL']);
      })
      return resultsArr;
    }
  }
];

/**
 * get GET attributes from url
 * @param data
 * @returns {{type: number, value: string}}
 */
function getDataFromUrl(data){
 let dataObj = {type: 0, value: ''};

  if (data.type !== undefined && data.type !== null && data.searchValue !== undefined && data.searchValue !== null){
    dataObj = {
      type: data.type * 1, // parse int
      searchValue: data.searchValue
    }
  }
  return dataObj;
}

/**
 * get url
 * @param data
 * @returns {*|string}
 */
function getUrl(data){
  return imageDataRepo.find(obj => obj.type === data.type).getUrl(data.searchValue);
}

/**
 *
 * @param data
 * @param type
 * @returns {*|Array}
 */
function fetchResults(data, type){
  return imageDataRepo.find(obj => obj.type === type).fetchResults(data);
}

/**
 * add headers - cors fixes
 */
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", appUrl);
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

/**
 * request to
 */
app.get(apiUrl, (req, res, next) => {
  let dataFromUrl = getDataFromUrl(req.query);

  if(dataFromUrl.type !== 0) {
    let url = getUrl(dataFromUrl);

    axios.get(url)
      .then(response => {
        let data = fetchResults(response.data, dataFromUrl.type);
        res.json(data);
      })
      .catch(error => {
        console.log(error);
      })
  }else{
    res.json([]);
  }
})

app.listen(3000);